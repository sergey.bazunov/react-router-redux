import React from 'react'
import { useEffect } from 'react';
import { useDispatch } from 'react-redux';
import { increment } from '../Actions/counterActions';

function NotFound(props) {
    const dispatch = useDispatch();

    useEffect(() => {
        dispatch(increment());
        }, []
    )    
    
    return (        
        <h1 className='my-3'>You've found NotFound page</h1>        
    );
}

export default NotFound;