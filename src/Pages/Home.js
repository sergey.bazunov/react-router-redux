import React from 'react'
import { useEffect } from 'react';
import { useDispatch } from 'react-redux';
import { increment } from '../Actions/counterActions';

function Home() {    
    const dispatch = useDispatch();

    useEffect(() => {
        dispatch(increment());
        }, []
    )    
    
    return (        
        <h1 className='my-3'>Welcome to Home page</h1>        
    );
}
export default Home