import React from 'react'
import { useSelector } from 'react-redux';
import { Badge } from 'reactstrap';


function Counter(props) {    
    //const context = useContext(CountStateContext);    
    //const value = useSelector(state => context.getState())
    const count = useSelector(state => state);
    return (
          <p className="m-3">You visit <Badge color="secondary">{count}</Badge> pages on this site</p>
    )
}

export default Counter;