import './App.css';
import 'bootstrap/dist/css/bootstrap.min.css';
import { Route, Link, Routes } from 'react-router-dom'

import { Nav, NavItem } from 'reactstrap';

import Home from './Pages/Home'
import Login from './Pages/Login'
import Register from './Pages/Register'
import NotFound from './Pages/NotFound'
import Counter from './Pages/Counter';

function App() {
  return (    
      <div className='px-5 pt-5'>                 

          <Nav tabs>
            <NavItem><Link className='nav-link' to="/">Home</Link></NavItem>
            <NavItem><Link className='nav-link' to="/login">Login</Link></NavItem>
            <NavItem><Link className='nav-link' to="/register">Register</Link></NavItem>
            <NavItem><Link className='nav-link' to="/404">404</Link></NavItem>
          </Nav>

          <Counter />

          <Routes>
            <Route path="/" element={<Home />} />
            <Route path='/login' element={<Login />}></Route>
            <Route path='/register' element={<Register />}></Route>
            <Route path='/404' element={<NotFound />}></Route>
          </Routes>      


      </div>
  );
}

export default App;
